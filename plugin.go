package main

import (
	"log"
	"encoding/json"
	"io/ioutil"
	"net/url"
	"bytes"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/gotify/plugin-api"

	"gitlab.com/Sorunome/matrix-gotify/api"
)

// GetGotifyPluginInfo returns gotify plugin info.
func GetGotifyPluginInfo() plugin.Info {
	return plugin.Info{
		ModulePath:  "gitlab.com/Sorunome/matrix-gotify",
		Version:     "0.0.1",
		Author:      "Sorunome",
		Website:     "https://gitlab.com/Sorunome/matrix-gotify",
		Description: "Receive notifications from matrix",
		License:     "Apache-2.0",
		Name:        "Sorunome/matrix-gotify",
	}
}

// Config is the configuration
type Config struct {
	Secret string
}

// Plugin is the gotify plugin instance.
type Plugin struct {
	msgHandler plugin.MessageHandler
	basePath string
	config *Config
	handledEventIds map[string]bool
}

// Enable enables the plugin.
func (c *Plugin) Enable() error {
	return nil
}

// Disable disables the plugin.
func (c *Plugin) Disable() error {
	return nil
}

// GetDisplay implements plugin.Displayer
func (c *Plugin) GetDisplay(baseURL *url.URL) string  {
	doc := bytes.NewBuffer([]byte{})
	doc.WriteString("# Matrix Notification Plugin\n\n")

	baseURL.Path = c.basePath
	hookURL := &url.URL{
		Path: "hook",
	}
	hookURL = baseURL.ResolveReference(hookURL)
	doc.WriteString(fmt.Sprintf("Webhook URI: %s\n\n", hookURL))

	if c.config.Secret == "" {
		doc.WriteString("**WARNING**: No secret set, plugin won't work.\n\n")
	}

	return doc.String()
}

// SetMessageHandler implements plugin.Messenger
// Invoked during initialization
func (c *Plugin) SetMessageHandler(h plugin.MessageHandler) {
	c.msgHandler = h
}

// DefaultConfig implements plugin.Configurer
// The default configuration will be provided to the user for future editing. Also used for Unmarshaling.
// Invoked whenever an unmarshaling is required.
func (c *Plugin) DefaultConfig() interface{} {
	return &Config{
		Secret: "",
	}
}

func (c *Plugin) ValidateAndSetConfig(conf interface{}) error {
	config := conf.(*Config)
	c.config = config
	return nil
}

func (c *Plugin) notify(title string, msg string, priority int) error {
	return c.msgHandler.SendMessage(plugin.Message{
		Title: title,
		Message: msg,
		Priority: priority,
		Extras: map[string]interface{}{
			"client::display": struct{
				ContentType string `json:"contentType"`
			}{"text/markdown"},
		},
	})
}

func (c *Plugin) notifyRoom(title string, msg string, roomId string, priority int) error {
	return c.msgHandler.SendMessage(plugin.Message{
		Title: title,
		Message: msg,
		Priority: priority,
		Extras: map[string]interface{}{
			"client::display": struct{
				ContentType string `json:"contentType"`
			}{"text/markdown"},
			"client::notification": struct{
				Click struct {
					Url string `json:"url"`
				} `json:"click"`
			}{struct{Url string `json:"url"`}{"https://matrix.to/#/" + roomId}},
		},
	})
}

func (c *Plugin) sendNotification(notification api.MatrixNotification) {
	log.Println("================")
	log.Println(notification.RoomId)
	log.Println(notification.EventId)
	if notification.EventId == "" || notification.RoomId == "" {
		// TODO: clear all current notifications
		return
	}
	if c.handledEventIds == nil {
		c.handledEventIds = make(map[string]bool)
	}
	if c.handledEventIds[notification.EventId] {
		// no duplicate notifications
		return
	}
	c.handledEventIds[notification.EventId] = true
	title := "New Message"
	if notification.RoomAlias != "" {
		title = notification.RoomAlias
	}
	if notification.RoomName != "" {
		title = notification.RoomName
	}
	if (notification.Sender != "" || notification.SenderDisplayName != "") && notification.Type != "" {
		sender := notification.Sender
		if notification.SenderDisplayName != "" {
			sender = notification.SenderDisplayName
		}
		if sender == "" {
			sender = "Someone"
		}
		if notification.Type == "m.room.message" {
			message := sender + " sent something"
			if notification.Content.Msgtype == "m.image" {
				message = sender + " sent an image"
			} else if notification.Content.Msgtype == "m.video" {
				message = sender + " sent a video"
			} else if notification.Content.Msgtype == "m.audio" {
				message = sender + " sent an audiofile"
			} else if notification.Content.Msgtype == "m.file" {
				message = sender + " sent a file"
			} else if notification.Content.Msgtype == "m.emote" {
				message = "*" + sender + " " + notification.Content.Body
			} else if notification.Content.Body != "" {
				message = sender + ": " + notification.Content.Body
			}
			c.notifyRoom(title, message, notification.RoomId, 5)
		} else if notification.Type == "m.room.encrypted" {
			c.notifyRoom(title, sender + " sent an encrypted message", notification.RoomId, 5)
		}
	} else {
		c.notifyRoom(title, "", notification.RoomId, 5)
	}
}

// RegisterWebhook implements plugin.Webhooker.
func (c *Plugin) RegisterWebhook(basePath string, g *gin.RouterGroup) {
	c.basePath = basePath
	g.POST("/hook", func(ctx *gin.Context) {
		if c.config.Secret == "" {
			log.Println("No secret set, aborting")
			// TODO: reject device
			ctx.AbortWithStatusJSON(500, nil)
			return
		}
		jsonData, err := ioutil.ReadAll(ctx.Request.Body)
		if err != nil {
			log.Println("Error reading message")
			log.Println(err)
			ctx.AbortWithStatusJSON(500, err.Error())
			return
		}

		res := new(api.MatrixNotificationPayload)
		if err := json.Unmarshal(jsonData, res); err != nil {
			log.Println("Error decoding json")
			log.Println(err)
			ctx.AbortWithStatusJSON(500, err.Error())
			return
		}
		var rejected []string
		for _, device := range res.Notification.Devices {
			if device.Pushkey == c.config.Secret {
				c.sendNotification(res.Notification)
			} else {
				rejected = append(rejected, device.Pushkey)
			}
		}
		bytes, err := json.Marshal(struct{
			Rejected []string `json:"rejected"`
		}{rejected})
		if err != nil {
			log.Println("Error encoding json")
			log.Println(err)
			ctx.AbortWithStatusJSON(500, err.Error())
			return
		}
		ctx.JSON(200, string(bytes))
	})
}

// NewGotifyPluginInstance creates a plugin instance for a user context.
func NewGotifyPluginInstance(ctx plugin.UserContext) plugin.Plugin {
	return &Plugin{}
}

func main() {
	panic("this should be built as go plugin")
}
