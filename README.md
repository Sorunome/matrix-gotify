# Matrix Gotify
This is a [gotify/server](https://github.com/gotify/server) plugin that is capable of receiving push
notifications from matrix and forwards them accordingly.

## Compiling
You need to have go and docker installed. You also need to have `~/go/bin` in your `$PATH`.

```bash
make download-tools
make build
```

Then copy the correct `.so` file in the build folder to the gotify server plugin folder.

## Usage
You need to configure the `secret` to be a random secret, e.g. the output of `uuidgen -r`.

Afterwards you need to set a pusher via curl. The corresponding endpoint in the client-server API
can be found [here](https://matrix.org/docs/spec/client_server/latest#post-matrix-client-r0-pushers-set).
An example payload can be found as `pusher_example_payload.json`, you'll just have to fill out your
secret and your pusher url.

Please note that if gotify runs on the same server as your homeserver the pusher url may be a localhost
url.

Example:

```bash
curl -H 'Authorization: Bearer -access-token-' -H 'Content-Type: application/json' -X POST -d '{"lang": "en","kind": "http","app_display_name": "Gotify","device_display_name": "Gotify","pushkey": "-configured-secret-","app_id": "de.sorunome.gotify","data": {"url": "http://localhost:8004/-plugin-path-/hook","format": "full_event"}}' https://sorunome.de/_matrix/client/r0/pushers/set
```
